﻿using System.Collections;
using System.Collections.Generic;
using HutongGames.PlayMaker.Actions;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class ZombieTest : MonoBehaviour
{

    public Rigidbody ArmL;

    public Rigidbody ArmR;

    public float Force;

    public float NeckLevelLimit = 0.5f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

	    if (Input.GetKeyDown(KeyCode.E))
	    {

	        if (ArmL!=null)
	            if (ArmL.transform.localPosition.x>NeckLevelLimit)
	                ArmL.AddForce(new Vector3(0f,Force,0f));

	        if (ArmR!=null)
	            if (ArmR.transform.localPosition.x>NeckLevelLimit)
	                ArmR.AddForce(new Vector3(0f,Force,0f));
	    }

	    if (Input.GetKeyDown((KeyCode.D)))
	    {
	        if (ArmL!=null)
	            ArmL.AddForce(new Vector3(0f,-Force,0f));
	        if (ArmR!=null)
	            ArmR.AddForce(new Vector3(0f,-Force,0f));
	    }

	    //float vAxis=CrossPlatformInputManager.GetAxis("Vertical");
	    //if (vAxis>0)
	    //    ArmL.AddForce(new Vector3(0f,10f,200f));
	    //else ArmL.AddForce(new Vector3(0f,-10f,-200f));
	}
}
