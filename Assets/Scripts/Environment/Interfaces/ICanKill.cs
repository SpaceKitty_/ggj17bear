﻿using JetBrains.Annotations;
using UnityEngine;

namespace Environment.Interfaces
{
    public interface ICanKill
    {

        bool DoesDamage { get; set; }
        int HitPoints { get; set; }
    }
}
