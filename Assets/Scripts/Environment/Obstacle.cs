﻿using Environment.Interfaces;
using UnityEngine;

namespace Environment
{
    /// <summary>
    /// Obstacle objects that kill the bear
    /// </summary>
    public class Obstacle : MonoBehaviour, ICanKill
    {
        public Collider ObstacleCollider;

        // Use this for initialization
        void Start () {
            if (ObstacleCollider == null) ObstacleCollider = this.GetComponent<Collider>();
        }

        void Awake()
        {

        }
	
        // Update is called once per frame
        void Update () {

        }

        void FixedUpdate()
        {

        }

        void OnCollisionEnter(Collision c)
        {
            Debug.Log(c.collider.gameObject.tag);
        }

        [SerializeField]
        public bool DoesDamage { get; set; }
        public int HitPoints { get; set; }
    }
}
