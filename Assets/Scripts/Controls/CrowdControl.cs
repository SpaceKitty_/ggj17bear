﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class CrowdControl : MonoBehaviour
{
    public List<Transform> TestCrowd;

    public float Elevation;

	// Use this for initialization
	void Start () {

	}
	
    void FixedUpdate()
    {
        // Read the inputs
        float vAxis=CrossPlatformInputManager.GetAxis("Vertical");

        Debug.Log("VAxis: "+vAxis);

        if (vAxis>0)
        foreach (var cube in TestCrowd)
        {
            cube.transform.localPosition=new Vector3(cube.transform.localPosition.x,
            cube.transform.localPosition.y + Elevation, cube.transform.localPosition.z);
        }

        if (vAxis < 0)
        {
            foreach (var cube in TestCrowd)
            {
                cube.transform.localPosition=new Vector3(cube.transform.localPosition.x,
                    cube.transform.localPosition.y - Elevation, cube.transform.localPosition.z);
            }
        }


        //float h = CrossPlatformInputManager.GetAxis("Horizontal");
        // Pass all parameters to the character control script.
        //if (CharacterRigidbody==null) CharacterRigidbody = UJS.CentralPoint.Body2D;
        //CharacterRigidbody.velocity = new Vector2(h * 6.0f, CharacterRigidbody.velocity.y);
        //m_Character.Move(h, crouch, m_Jump);

        //float v = CrossPlatformInputManager.GetAxis("Vertical");
        //if (v < 0.0f)
        //{
        //    Debug.Log("Down");
        //    if (CharacterRigidbody == null) CharacterRigidbody = UJS.CentralPoint.Body2D;
        //    CharacterRigidbody.AddForce(new Vector2(0f, v*1500f));
        //}
    }

}
