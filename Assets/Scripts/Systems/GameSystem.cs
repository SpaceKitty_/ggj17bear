﻿using UnityEngine;

namespace Systems
{
    public class GameSystem : MonoBehaviour {

        public enum GameState
        {
            Running =0 ,
            GameOver = 1
        }

        public GameState CurrentGameState;
        
        // Use this for initialization
        void Start ()
        {
            CurrentGameState = GameState.Running;
        }
	
        // Update is called once per frame
        void Update () {
		
        }
    }
}
