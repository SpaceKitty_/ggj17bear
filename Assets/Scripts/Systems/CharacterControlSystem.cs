using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Assets.Scripts.Systems
{
    public class CharacterControlSystem : MonoBehaviour
    {

        public Rigidbody2D CharacterRigidbody;

        public UnityJellySprite UJS;

        // Use this for initialization
        void Start () {

        }
	
        // Update is called once per frame
        void Update () {
                // Read the jump input in Update so button presses aren't missed.
            if (CrossPlatformInputManager.GetButtonDown("Jump"))
            {
                Debug.Log("Jump");

                //if (CharacterRigidbody == null) CharacterRigidbody = UJS.CentralPoint.Body2D;
                //CharacterRigidbody.AddForce(new Vector2(0f, 2800f));
            }

        }

        void FixedUpdate()
        {
            // Read the inputs
            bool crouch = Input.GetKey(KeyCode.LeftControl);
		
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            // Pass all parameters to the character control script.
            //if (CharacterRigidbody==null) CharacterRigidbody = UJS.CentralPoint.Body2D;
            //CharacterRigidbody.velocity = new Vector2(h * 6.0f, CharacterRigidbody.velocity.y);
            //m_Character.Move(h, crouch, m_Jump);

            float v = CrossPlatformInputManager.GetAxis("Vertical");
            Debug.Log(v);
            if (v < 0.0f)
            {
                Debug.Log("Down");
                if (CharacterRigidbody == null) CharacterRigidbody = UJS.CentralPoint.Body2D;
                CharacterRigidbody.AddForce(new Vector2(0f, v*1500f));
            }
        }
    }
}
