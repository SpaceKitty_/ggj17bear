﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTest : MonoBehaviour
{

    public Rigidbody BallBody;

    public float ForceMove;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	    BallBody.AddForce(new Vector2(ForceMove, 0f));
	}
}
