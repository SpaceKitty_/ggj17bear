﻿using JetBrains.Annotations;
using UnityEngine;

namespace Characters.Interfaces
{
    public interface ICanDie
    {

        void TakeDamage();
        void Die();

    }
}
